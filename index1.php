<?php

$amount = 178;

function get_change(int $amount): array {
  $coins_type = [10, 5, 2, 1];
  $result = [0, 0, 0, 0];

  foreach ($coins_type as $key => $type) {
    $operation = ($amount - ($amount % $coins_type[$key])) / $coins_type[$key];
    $result[$key] = $operation;
    $amount -= $operation * $coins_type[$key];
  }
  $render = [ "bill10" => $result[0], "bill5" => $result[1], "coin2" => $result[2], "coin1" => $result[3] ];
  return $render;
}

print_r(get_change($amount));