<?php

$amount = 18;

function get_change(int $amount, bool $isCoin1 = true): array {
    if ($isCoin1 === true) {
      $coins_type = [10, 5, 2, 1];
    $result = [ "bill10" => 0, "bill5" => 0, "coin2" => 0, "coin1" => 0 ];
    foreach ($coins_type as $key => $type) {
      $operation = ($amount - ($amount % $type)) / $type;
      if ($type >= 5) {
        $result["bill{$type}"] = $operation;
      } else {
        $result["coin{$type}"] = $operation;
      }
      $amount -= $operation * $type;
    }
    return $result;
  } else {
    if ($amount === 1 || $amount === 3) {
      trigger_error("can't give change back", E_USER_ERROR);
    }
    $coins_type = [10, 2];
    $result = [ "bill10" => 0, "bill5" => 0, "coin2" => 0];
    if ($amount % 2 !== 0) {
      $result["bill5"] = 1;
      $amount -= 5;
    }
    foreach ($coins_type as $key => $type) {
      $operation = ($amount - ($amount % $type)) / $type;
      if ($type >= 5) {
        $result["bill{$type}"] += $operation;
      } else {
        $result["coin{$type}"] += $operation;
      }
      $amount -= $operation * $type;
    }
    return $result;
  }
}

print_r(get_change($amount, false));