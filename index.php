<?php

$amount = 178;

function get_change(int $amount): array {
  $coins_type = [10, 5, 2, 1];
  $result = [0, 0, 0, 0];
  $sum = 0;
  while ($sum < $amount) {
    if ($amount - $sum >= $coins_type[0]) {
      $sum += $coins_type[0];
      $result[0]++;
    } else if ($amount - $sum  >= $coins_type[1]) {
      $sum += $coins_type[1];
      $result[1]++;
    } else if ($amount - $sum  >= $coins_type[2]) {
      $sum += $coins_type[2];
      $result[2]++;
    } else if ($amount - $sum  >= $coins_type[3]) {
      $sum += $coins_type[3];
      $result[3]++;
    }
  }
  $render = [ "bill10" => $result[0], "bill5" => $result[1], "coin2" => $result[2], "coin1" => $result[3] ];
  return $render;
}

print_r(get_change($amount));